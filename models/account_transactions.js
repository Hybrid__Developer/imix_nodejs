
'use strict'
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('account_transactions', {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        client_id: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false
        },
        account_id: {
            type: DataTypes.STRING,
            allowNull: false
        },
        capital_addition: {
            type: DataTypes.DOUBLE(8, 2),
            allowNull: true,
            defaultValue: 0.0
        },
        capital_withdraw: {
            type: DataTypes.DOUBLE(8, 2),
            allowNull: true,
            defaultValue: 0.0
        },
        transaction_date: DataTypes.DATE,
        opening_value: {
            type: DataTypes.DOUBLE(8, 2),
            allowNull: true,
            defaultValue: 0.0
        },
        gain_loss: {
            type: DataTypes.DOUBLE(8, 2),
            allowNull: true,
            defaultValue: 0.0
        },
        created_at: DataTypes.DATE
    }, {
        freezeTableName: true
    })
}