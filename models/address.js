
'use strict'
// var bcrypt = require('bcrypt-nodejs')
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('addresses', {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        client_id: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false
        },
        address_type: {
            type: DataTypes.STRING,
            allowNull: true,
            defaultValue: ''
        },
        address1: {
            type: DataTypes.STRING({ length: 200 }),
            allowNull: true,
            defaultValue: ''
        },
        address2: {
            type: DataTypes.STRING({ length: 200 }),
            allowNull: true,
            defaultValue: ""
        },
        country: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: ""
        },
        city: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: ""
        },
        post_code: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: ""
        },
        created_at: DataTypes.DATE,
        deleted_at: {
            type: DataTypes.DATE,
            allowNull: true,
            defaultValue: null
        }
    }, {
        freezeTableName: true
    })
}