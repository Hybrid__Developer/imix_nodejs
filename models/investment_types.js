
'use strict'
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('investment_types', {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        name: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        holding: {
            type: DataTypes.DOUBLE(8, 2),
            allowNull: true,
            defaultValue: 0.0
        },
        holding_today: {
            type: DataTypes.DOUBLE(8, 2),
            allowNull: true,
            defaultValue: 0.0
        },
        market_value: {
            type: DataTypes.DOUBLE(8, 2),
            allowNull: true,
            defaultValue: 0.0
        },
        changes: {
            type: DataTypes.DOUBLE(8, 2),
            allowNull: true,
            defaultValue: 0.0
        },
        created_at: DataTypes.DATE
    }, {
    freezeTableName: true
    })
}