// 'use strict'

// const fs = require('fs')
// const path = require('path')
// const mongoose = require('mongoose')

// const configure = async () => {
//     const files = fs.readdirSync(path.join(__dirname))

//     for (let file of files) {
//         const fileName = file.split('.').slice(0, -1).join('.')
//         if (fileName && fileName !== 'index') {
//             mongoose.model(fileName, require(`./${fileName}`))
//         }
//     }
// }

// exports.configure = configure
'use strict'
const fs = require('fs')
const path = require('path')
const basename = path.basename(module.filename)
const lodash = require('lodash')

let initModels = () => {
    let db = {}
    fs.readdirSync(__dirname)
        .filter((file) => {
            return (file.indexOf('.') !== 0) && (file !== basename)
        })
        .forEach((file) => {
            let model = sequelize['import'](path.join(__dirname, file))
            db[model.name] = model
        })


    // db.clients.hasMany(db.client_paymethods)

    // db.client_paymethods.belongsTo(db.clients)

    // db.clients.hasMany(db.orders)
    // db.orders.belongsTo(db.clients)

    // db.orders.hasMany(db.carts)
    // db.carts.belongsTo(db.orders)

    // db.carts.hasOne(db.menus)
    // db.menus.belongsTo(db.carts)

    // db.menus.hasMany(db.addon)
    // db.addon.belongsTo(db.menus)
    // db.menus.hasMany(db.carts)
    // db.carts.belongsTo(db.menus)
    // db.carts.hasOne(db.stores)
    // db.stores.belongsTo(db.carts)
    // db.favorites.belongsTo(db.reportRequest)
    db.investment_types.hasMany(db.investments)
    db.investments.belongsTo(db.investment_types)

    Object.keys(db).forEach((modelName) => {
        if ('associate' in db[modelName]) {
            db[modelName].associate(db)
        }
    })
    return db
}

module.exports = lodash.extend({
    sequelize: sequelize,
    Sequelize: Sequelize
}, initModels())