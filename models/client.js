
'use strict'
// var bcrypt = require('bcrypt-nodejs')
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('clients', {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        client_id: {
            type: DataTypes.STRING({ length: 50 }),
            allowNull: true,
            defaultValue: ''
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: ''
        },
        username: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: ''
        },
        image: {
            type: DataTypes.STRING({ length: 200 }),
            allowNull: true,
            defaultValue: ""
        },
        memorable: {
            type: DataTypes.STRING({ length: 200 }),
            allowNull: false
        },
        email: {
            type: DataTypes.STRING,
            validate: {
                isEmail: true
            },
            unique: true
        },
        search_key: {
            type: DataTypes.STRING,
            allowNull: true,
            defaultValue: ''
        },
        currency: {
            type: DataTypes.DOUBLE(8, 2),
            allowNull: true,
            defaultValue: 0.0
        },
        nationality: {
            type: DataTypes.STRING({ length: 200 }),
            allowNull: true
        },
        gender: {
            type: DataTypes.STRING({ length: 200 }),
            allowNull: true
        },
        dob: {
            type: DataTypes.DATE,
            allowNull: true
        },
        cgt_enabled: {
            type: DataTypes.STRING({ length: 200 }),
            allowNull: true
        },
        start_date: {
            type: DataTypes.DATE,
            allowNull: true
        },
        closed_date: {
            type: DataTypes.DATE,
            allowNull: true
        },
        inception_date: {
            type: DataTypes.DATE,
            allowNull: true
        },
        mifid_type: {
            type: DataTypes.STRING({ length: 200 }),
            allowNull: true
        },
        mifid_category: {
            type: DataTypes.STRING({ length: 200 }),
            allowNull: true
        },
        ni_number: {
            type: DataTypes.STRING({ length: 200 }),
            allowNull: true
        },
        deceased_date: {
            type: DataTypes.DATE,
            allowNull: true
        },
        marital_status: {
            type: DataTypes.STRING({ length: 200 }),
            allowNull: true
        },
        w8ben: {
            type: DataTypes.STRING({ length: 200 }),
            allowNull: true
        },
        revision_marker: {
            type: DataTypes.STRING({ length: 200 }),
            allowNull: true
        },
        tax_rate: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        risk_profile_id: {
            type: DataTypes.STRING({ length: 200 }),
            allowNull: true
        },
        client_type: {
            type: DataTypes.STRING({ length: 200 }),
            allowNull: true
        },
        mifid_identifier_type: {
            type: DataTypes.STRING({ length: 200 }),
            allowNull: true
        },
        lei: {
            type: DataTypes.STRING({ length: 200 }),
            allowNull: true
        },
        national_id: {
            type: DataTypes.STRING({ length: 200 }),
            allowNull: true
        },
        concat: {
            type: DataTypes.STRING({ length: 200 }),
            allowNull: true
        },
        password: DataTypes.STRING,
        created_at: DataTypes.DATE
    }, {
        freezeTableName: true
    })
}