
'use strict'
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('investments', {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        client_id: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false
        },
        account_id: {
            type: DataTypes.STRING,
            allowNull: false
        },
        investmentTypeId: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: true,
        },
        investment_in: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        investment_date: DataTypes.DATE,

        market_value: {
            type: DataTypes.DOUBLE(8, 2),
            allowNull: true,
            defaultValue: 0.0
        },

        holding: {
            type: DataTypes.DOUBLE(8, 2),
            allowNull: true,
            defaultValue: 0.0
        },

        holding_today: {
            type: DataTypes.DOUBLE(8, 2),
            allowNull: true,
            defaultValue: 0.0
        },

        changes: {
            type: DataTypes.DOUBLE(8, 2),
            allowNull: true,
            defaultValue: 0.0
        },
        book_cost: {
            type: DataTypes.DOUBLE(8, 2),
            allowNull: true,
            defaultValue: 0.0
        },
        projected_yield: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: true
        },
        created_at: DataTypes.DATE
    }, {
        freezeTableName: true
    })
}