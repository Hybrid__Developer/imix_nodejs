
'use strict'
// var bcrypt = require('bcrypt-nodejs')
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('accounts', {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        client_id: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false
        },
        account_id: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        account_name: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        company_id: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        account_type_id: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        risk_profile_id: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        settlement_currency: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        start_date: DataTypes.DATE,
        closed_date: DataTypes.DATE,
        investment_type: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        total_amount: {
            type: DataTypes.DOUBLE(8, 2),
            allowNull: true,
            defaultValue: 0.0
        },
        fee_date: DataTypes.DATE,
        performance_date: DataTypes.DATE,
        commission1_id: {
            type: DataTypes.STRING,
            allowNull: true
        },
        commission2_id: {
            type: DataTypes.STRING,
            allowNull: true
        },
        commission3: {
            type: DataTypes.STRING,
            allowNull: true
        },
        cash_buffer_value: {
            type: DataTypes.STRING,
            allowNull: true
        },
        cash_buffer_percent: {
            type: DataTypes.STRING,
            allowNull: true
        },
        exclude_from_re_balancing: {
            type: DataTypes.STRING,
            allowNull: true
        },
        reporting_currency: {
            type: DataTypes.STRING,
            allowNull: true
        },
        depot_code: {
            type: DataTypes.STRING,
            allowNull: true
        },
        model_code: {
            type: DataTypes.STRING,
            allowNull: true
        },
        closed: {
            type: DataTypes.STRING,
            allowNull: true
        },
        revision_marker: {
            type: DataTypes.STRING,
            allowNull: true
        },
        is_cgt: {
            type: DataTypes.STRING,
            allowNull: true
        },
        has_w8_ben_form	: {
            type: DataTypes.STRING,
            allowNull: true
        },
        risk_score: {
            type: DataTypes.STRING,
            allowNull: true
        },
        index_id: {
            type: DataTypes.STRING,
            allowNull: true
        },
        asset_allocation_profile_id: {
            type: DataTypes.STRING,
            allowNull: true
        },
        sofac_package_id: {
            type: DataTypes.STRING,
            allowNull: true
        },
        account_management_id: {
            type: DataTypes.STRING,
            allowNull: true
        },
        asset_allocation_profile_id2: {
            type: DataTypes.STRING,
            allowNull: true
        },
        asset_allocation_profile_id3: {
            type: DataTypes.STRING,
            allowNull: true
        },
        asset_allocation_profile_id4: {
            type: DataTypes.STRING,
            allowNull: true
        },
        asset_allocation_profile_id5: {
            type: DataTypes.STRING,
            allowNull: true
        },
        decision_maker_type: {
            type: DataTypes.STRING,
            allowNull: true
        },
        decision_maker_value: {
            type: DataTypes.STRING,
            allowNull: true
        },
        order_route_id: {
            type: DataTypes.STRING,
            allowNull: true
        },
        created_at: DataTypes.DATE
    }, {
        freezeTableName: true
    })
}