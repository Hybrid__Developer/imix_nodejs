'use strict'
const clientService = require('../services/clients')
const response = require('../exchange/response')
const mapper = require('../mappers/clients')

const create = async (req, res) => {
    const log = req.context.logger.start(`api:clients:create`)
    try {
        const message = await clientService.create(req.body, req.context)
        log.end()
        return response.success(res, 'User Registered successfully')
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}


const login = async (req, res) => {
    const log = req.context.logger.start('api:clients:login')
    try {
        const client = await clientService.login(req.body, req.context)
        log.end()
        return response.data(res, mapper.toModel(client));
        //return response.success(res, client)
    } catch (err) {
        log.error(err)
        log.end()
        return response.failure(res, err.message)
    }
}

const addAddress = async (req, res) => {
    const log = req.context.logger.start(`api:clients:addAddress`)
    try {
        const message = await clientService.addAddress(req.body, req.context)
        log.end()
        res.message = 'Address add successfully'
        return response.data(res, message)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const updateAddress = async (req, res) => {
    const log = req.context.logger.start(`api/clients/updateAddress/${req.params.id}`)
    try {
        const address = await clientService.updateAddress(req.params.id, req.body, req.context)
        log.end()
        return response.data(res, address)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const addressList = async (req, res) => {
    const log = req.context.logger.start(`api/clients/addressList/${req.params.id}`)
    try {
        const address = await clientService.addressList(req.params.id, req.body, req.context)
        log.end()
        return response.data(res, address)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
const forgotPasswordOtp = async (req, res) => {
    const log = req.context.logger.start(`api/clients/forgotPasswordOtp`)
    try {
        const forgotOtp = await clientService.forgotPasswordOtp(req.body, req.context)
        log.end()
        return response.data(res, forgotOtp)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
const resetPassword = async (req, res) => {
    const log = req.context.logger.start(`api/clients/resetPassword`)
    try {
        const forgotData = await clientService.resetPassword(req.body, req.context)
        log.end()
        res.message = 'Password set successfully.'
        //return response.data(res, forgotData)
        return response.data(res, mapper.toModel(forgotData));
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
const changePassword = async (req, res) => {
    const log = req.context.logger.start(`api/clients/changePassword`)
    try {
        const passwordData = await clientService.changePassword(req.body, req.context)
        log.end()
        res.message = 'Password change successfully.'
        return response.data(res, passwordData)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}



const update = async (req, res) => {
    const log = req.context.logger.start(`api/clients/update/${req.params.id}`)
    try {
        const client = await clientService.update(req.params.id, req.body, req.context)
        log.end()
        return response.data(res, client)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const imageUploader = async (req, res) => {
    try {
        let log = req.context.logger.start(`api:clients:imageUploader/${req.params.id}`)
        const client = await clientService.imageUploader(req, req.params.id, req.context)
        res.message = 'ProfilePic Uploaded Successfully.'
        return response.data(res, { image: client.image })
    } catch (err) {
        response.failure(res, err.message)
    }
}
const deleteAddress = async (req, res) => {
    const log = req.context.logger.start(`api/clients/deleteAddress/${req.params.id}`)
    try {
        const address = await clientService.deleteAddress(req.params.id, req.body, req.context)
        log.end()
        return response.data(res, address)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.login = login
exports.create = create
exports.update = update
exports.imageUploader = imageUploader
exports.addAddress = addAddress
exports.updateAddress = updateAddress
exports.addressList = addressList
exports.resetPassword = resetPassword
exports.forgotPasswordOtp = forgotPasswordOtp
exports.changePassword = changePassword
exports.deleteAddress = deleteAddress