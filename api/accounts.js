'use strict'
const accountService = require('../services/accounts')
const response = require('../exchange/response')
const mapper = require('../mappers/accounts')
const investmentsMapper = require('../mappers/investments')

//Consolidated Home screen
const consolidateAccount = async (req, res) => {
    const log = req.context.logger.start(`api/accounts/consolidateAccount/${req.params.id}`)
    try {
        const accounts = await accountService.consolidateAccount(req.params.id, req.body, req.context)
        log.end()
        return response.data(res, mapper.toSearchModel(accounts));
        //return response.data(res, accounts)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

//Consolidated Valuation screen
const consolidateValuation = async (req, res) => {
    const log = req.context.logger.start(`api/accounts/consolidateValuation/${req.params.id}`)
    try {
        const valuation = await accountService.consolidateValuation(req.params.id, req.body, req.context)
        log.end()
        // return response.data(res, valuation);
        return response.data(res, investmentsMapper.toSearchModel(valuation))
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

//Account Valuation screen
const accountValuation = async (req, res) => {
    const log = req.context.logger.start(`api/accounts/accountValuation/${req.params.id}`)
    try {
        const valuation = await accountService.accountValuation(req.params.id, req.body, req.context)
        log.end()
        return response.data(res, investmentsMapper.toSearchModel(valuation));
        //return response.data(res, valuation)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

//Consolidate assets Allocation screen
const consolidateAssets = async (req, res) => {
    const log = req.context.logger.start(`api/accounts/consolidateAssets`)
    try {
        const assets = await accountService.consolidateAssets(req.query, req.body, req.context)
        log.end()
        return response.data(res, assets)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

//Account assets Allocation screen
const accountAssets = async (req, res) => {
    const log = req.context.logger.start(`api/accounts/accountAssets`)
    try {
        const accAssets = await accountService.accountAssets(req.query, req.body, req.context)
        log.end()
        return response.data(res, accAssets)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

//Consolidate performance screen
const consolidatePerformance = async (req, res) => {
    const log = req.context.logger.start(`api/accounts/consolidatePerformance`)
    try {
        const accPerform = await accountService.consolidatePerformance(req.query, req.body, req.context)
        log.end()
        return response.data(res, accPerform)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}



//Account performance screen
const accountPerformance = async (req, res) => {
    const log = req.context.logger.start(`api/accounts/accountPerformance`)
    // const log = req.context.logger.start(`api/accounts/accountPerformance/${req.params.id}`)
    try {
        const accPerform = await accountService.accountPerformance(req.query, req.body, req.context)
        // const accPerform = await accountService.accountPerformance(req.params.id, req.body, req.context)
        log.end()
        return response.data(res, accPerform)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}





exports.consolidateAccount = consolidateAccount
exports.consolidateValuation = consolidateValuation
exports.accountValuation = accountValuation
exports.consolidateAssets = consolidateAssets
exports.accountAssets = accountAssets
exports.consolidatePerformance = consolidatePerformance
exports.accountPerformance = accountPerformance