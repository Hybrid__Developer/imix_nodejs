'use strict'
const encrypt = require('../permit/crypto')
const url = require('config').get('pic').url
var rootPath = require('path');
var fs = require('fs')
var nodemailer = require('nodemailer')

//Consolidated Home screen
const consolidateAccount = async (id, model, context) => {
    const log = context.logger.start(`services/accounts/consolidateAccount/model:${model}`)

    let isUserExist = await db.clients.find({
        where: {
            id: id,
        }
    })
    if (!isUserExist) {
        throw new Error('Sorry client is not found..')
    }

    let allAccounts = await db.accounts.findAll({
        where: {
            client_id: id,
        }
    })

    if (!allAccounts.length) {
        throw new Error('Sorry account is not found...')
    }
    // let total = 0
    // allAccounts.forEach(element =>
    //     total += element.total_amount
    //      );
    log.end()
    //allAccounts.consolidate = {consolidateAmt: total}
    return allAccounts
}

//Consolidated Valuation screen
const consolidateValuation = async (id, model, context) => {
    const log = context.logger.start(`services/accounts/consolidateValuation/model:${model}`)

    let isUserExist = await db.clients.find({

        where: {
            id: id,
        }
    })
    if (!isUserExist) {
        throw new Error('Sorry client is not found..')
    }
    let investments = db.investments
    let allValuations = await db.investment_types.findAll({
        include: [{
            model: db.investments, where: { client_id: id }, attributes: ['client_id', 'investment_in', 'market_value', 'book_cost', 'projected_yield'], required: false
        }]
    })

    if (!allValuations.length) {
        throw new Error('Sorry investment is not found...')
    }

    let marketGrandTotal = 0
    let bookGrandTotal = 0
    let yieldGrandTotal = 0

    allValuations.forEach((e) => {
        e.investments.forEach((elem) => {
            marketGrandTotal += elem.market_value
            bookGrandTotal += elem.book_cost
            yieldGrandTotal += elem.projected_yield
        });
    });
    yieldGrandTotal = ((marketGrandTotal - bookGrandTotal) / bookGrandTotal * 100).toFixed(2)
    let total = {
        marketGrandTotal: marketGrandTotal,
        bookGrandTotal: bookGrandTotal,
        yieldGrandTotal: yieldGrandTotal,
    }

    allValuations.push(total)

    log.end()
    return allValuations
}

//Account Valuation screen
const accountValuation = async (id, model, context) => {
    const log = context.logger.start(`services/accounts/accountValuation/model:${model}`)

    let isAccntExist = await db.accounts.find({
        where: {
            account_id: id,
        }
    })
    if (!isAccntExist) {
        throw new Error('Sorry account is not found..')
    }

    let allValuations = await db.investment_types.findAll({
        include: [{
            model: db.investments, where: { account_id: id }, attributes: ['client_id', 'investment_in', 'market_value', 'book_cost', 'projected_yield'], required: false
        }]
    })

    if (!allValuations.length) {
        throw new Error('Sorry investment is not found...')
    }

    let marketGrandTotal = 0
    let bookGrandTotal = 0
    let yieldGrandTotal = 0

    allValuations.forEach((e) => {
        e.investments.forEach((elem) => {
            marketGrandTotal += elem.market_value
            bookGrandTotal += elem.book_cost
            yieldGrandTotal += elem.projected_yield
        });
    });
    yieldGrandTotal = ((marketGrandTotal - bookGrandTotal) / bookGrandTotal * 100).toFixed(2)
    let total = {
        marketGrandTotal: marketGrandTotal,
        bookGrandTotal: bookGrandTotal,
        yieldGrandTotal: yieldGrandTotal,
    }
    allValuations.push(total)

    log.end()
    return allValuations
}


//Consolidate assets Allocation screen
const consolidateAssets = async (query, model, context) => {
    const log = context.logger.start(`services/accounts/consolidateAssets/model:${model}`)

    var id = query.id

    if (query.searchBy == 'YTD') {
        var filterBy = new Date().getFullYear()
        let isUserExist = await db.clients.find({
            where: {
                id: id,
            }
        })
        if (!isUserExist) {
            throw new Error('Sorry client is not found..')
        }

        let assets = await db.investment_types.findAll({
            include: {
                model: db.investments,
                where: { 
                    client_id: id,
                    year: filterBy
                }
            }
        })

        if (!assets.length) {
            throw new Error('Sorry investment is not found...')
        }

        let total = 0
        let market = 0
        // assets.forEach(element => {
        //     total += element.holding
        //     market += element.market_value
        // })
        assets.forEach((e) => {
            total += e.holding
            e.investments.forEach((elem) => {
                //console.log(elem.market_value)
                market += elem.market_value
            });
        });
    //assets.holdingTotal = total
    //assets.marketValueTotal = market,

        log.end()
        return {
            assets: assets,
            holdingTotal: total,
            marketValueTotal: market
        }
    }

        if (query.searchBy == '3Years') {
        var year = new Date(new Date().setFullYear(new Date().getFullYear() - 3));
        var filterBy = year.getFullYear()
        let isUserExist = await db.clients.find({
            where: {
                id: id,
            }
        })
        if (!isUserExist) {
            throw new Error('Sorry client is not found..')
        }

        let assets = await db.investment_types.findAll({
            include: {
                model: db.investments,
                where: { 
                    client_id: id,
                    year: {
                      $gte: filterBy,
                      $lte: new Date(),
                    },
                }
            }
        })

        if (!assets.length) {
            throw new Error('Sorry investment is not found...')
        }

        let total = 0
        let market = 0
        // assets.forEach(element => {
        //     total += element.holding
        //     market += element.market_value
        // })
        assets.forEach((e) => {
            total += e.holding
            e.investments.forEach((elem) => {
                //console.log(elem.market_value)
                market += elem.market_value
            });
        });
    //assets.holdingTotal = total
    //assets.marketValueTotal = market,

        log.end()
        return {
            assets: assets,
            holdingTotal: total,
            marketValueTotal: market
        }
    }

        if (query.searchBy == 'SinceInception') {
        let isUserExist = await db.clients.find({
            where: {
                id: id,
            }
        })
        if (!isUserExist) {
            throw new Error('Sorry client is not found..')
        }

        let assets = await db.investment_types.findAll({
            include: {
                model: db.investments,
                where: { 
                    client_id: id,
                }
            }
        })

        if (!assets.length) {
            throw new Error('Sorry investment is not found...')
        }

        let total = 0
        let market = 0
        // assets.forEach(element => {
        //     total += element.holding
        //     market += element.market_value
        // })
        assets.forEach((e) => {
            total += e.holding
            e.investments.forEach((elem) => {
                //console.log(elem.market_value)
                market += elem.market_value
            });
        });
    //assets.holdingTotal = total
    //assets.marketValueTotal = market,

        log.end()
        return {
            assets: assets,
            holdingTotal: total,
            marketValueTotal: market
        }
    }
}


//Account assets Allocation screen
const accountAssets = async (query, model, context) => {
    const log = context.logger.start(`services/accounts/accountAssets/model:${model}`)

    var id = query.id

    if (query.searchBy == 'YTD') {
        var filterBy = new Date().getFullYear()

        let isAccountExist = await db.accounts.find({
            where: {
                account_id: id,
                year: filterBy
            }
        })
        if (!isAccountExist) {
            throw new Error('Sorry account is not found..')
        }

        let accAssets = await db.investment_types.findAll({
            include: {
                model: db.investments,
                where: { 
                    account_id: id,
                    year: filterBy
                },
                attributes: ['account_id', 'client_id', 'market_value', 'investment_in','investment_date']
            }
        })

        if (!accAssets.length) {
            throw new Error('Sorry investment is not found...')
        }

        let total = 0
        let market = 0
        // accAssets.forEach(element => {
        //     total += element.holding
        //     market += element.market_value
        // })

        accAssets.forEach((e) => {
            total += e.holding
            e.investments.forEach((elem) => {
                //console.log(elem.market_value)
                market += elem.market_value
            });
        });
        // accAssets.push({
        //     holdingTotal: total,
        //     marketValueTotal: market,
        // });

        log.end()
        return {
            assets: accAssets,
            holdingTotal: total,
            marketValueTotal: market
        }
    }

    if (query.searchBy == '3Years') {
        var year = new Date(new Date().setFullYear(new Date().getFullYear() - 3));
        var filterBy = year.getFullYear()

        let isAccountExist = await db.accounts.find({
            where: {
                account_id: id,
                year: {
                  $gte: filterBy,
                  $lte: new Date(),
                },
            }
        })
        if (!isAccountExist) {
            throw new Error('Sorry account is not found..')
        }

        let accAssets = await db.investment_types.findAll({
            include: {
                model: db.investments,
                where: { 
                    account_id: id,
                    year: {
                      $gte: filterBy,
                      $lte: new Date(),
                    },
                },
                attributes: ['account_id', 'client_id', 'market_value', 'investment_in','investment_date']
            }
        })

        if (!accAssets.length) {
            throw new Error('Sorry investment is not found...')
        }

        let total = 0
        let market = 0
        // accAssets.forEach(element => {
        //     total += element.holding
        //     market += element.market_value
        // })

        accAssets.forEach((e) => {
            total += e.holding
            e.investments.forEach((elem) => {
                //console.log(elem.market_value)
                market += elem.market_value
            });
        });
        // accAssets.push({
        //     holdingTotal: total,
        //     marketValueTotal: market,
        // });

        log.end()
        return {
            assets: accAssets,
            holdingTotal: total,
            marketValueTotal: market
        }
    }

    if (query.searchBy == 'SinceInception') {

        let isAccountExist = await db.accounts.find({
            where: {
                account_id: id,
            }
        })
        if (!isAccountExist) {
            throw new Error('Sorry account is not found..')
        }

        let accAssets = await db.investment_types.findAll({
            include: {
                model: db.investments,
                where: { 
                    account_id: id,
                },
                attributes: ['account_id', 'client_id', 'market_value', 'investment_in','investment_date']
            }
        })

        if (!accAssets.length) {
            throw new Error('Sorry investment is not found...')
        }

        let total = 0
        let market = 0
        // accAssets.forEach(element => {
        //     total += element.holding
        //     market += element.market_value
        // })

        accAssets.forEach((e) => {
            total += e.holding
            e.investments.forEach((elem) => {
                //console.log(elem.market_value)
                market += elem.market_value
            });
        });
        // accAssets.push({
        //     holdingTotal: total,
        //     marketValueTotal: market,
        // });

        log.end()
        return {
            assets: accAssets,
            holdingTotal: total,
            marketValueTotal: market
        }
    }

}

//Consolidate performance screen
const consolidatePerformance = async (query, model, context) => {
    const log = context.logger.start(`services/accounts/consolidatePerformance/model:${model}`)

    var id = query.id

    if (query.searchBy == 'YTD') {
        var filterBy = new Date().getFullYear()
        let isClientExist = await db.clients.find({
            where: {
                id: id,
            }
        })
        if (!isClientExist) {
            throw new Error('Sorry client is not found..')
        }

        let accTransaction = await db.account_transactions.findAll({
            where: {
                client_id: id,
                year: filterBy
            }
        })

        if (!accTransaction.length) {
            throw new Error('Sorry transaction is not found...')
        }
        log.end()
        return accTransaction
    }

    if (query.searchBy == '3Years') {
        var year = new Date(new Date().setFullYear(new Date().getFullYear() - 3));
        var filterBy = year.getFullYear()

        let isClientExist = await db.clients.find({
            where: {
                id: id,
            }
        })
        if (!isClientExist) {
            throw new Error('Sorry client is not found..')
        }

        let accTransaction = await db.account_transactions.findAll({
            where: {
                client_id: id,
                year: {
                  $gte: filterBy,
                  $lte: new Date(),
                },
            }
        })

        if (!accTransaction.length) {
            throw new Error('Sorry transaction is not found...')
        }
        log.end()
        return accTransaction
    }

    if (query.searchBy == 'SinceInception') {

        let isClientExist = await db.clients.find({
            where: {
                id: id,
            }
        })
        if (!isClientExist) {
            throw new Error('Sorry client is not found..')
        }

        let accTransaction = await db.account_transactions.findAll({
            where: {
                client_id: id,
            }
        })

        if (!accTransaction.length) {
            throw new Error('Sorry transaction is not found...')
        }
        log.end()
        return accTransaction
    }
}

//Account performance screen
const accountPerformance = async (query, model, context) => {
    const log = context.logger.start(`services/accounts/accountPerformance/model:${model}`)

    var id = query.id

    if (query.searchBy == 'YTD') {
        var filterBy = new Date().getFullYear()
        let isAccountExist = await db.accounts.find({
            where: {
                account_id: id,
                year: filterBy
            }
        })
        if (!isAccountExist) {
            throw new Error('Sorry account is not found..')
        }

        let accTransaction = await db.account_transactions.findAll({
            where: {
                account_id: id,
                year: filterBy
            }
        })

        if (!accTransaction.length) {
            throw new Error('Sorry transaction is not found...')
        }

        log.end()

        return accTransaction
    }

    if (query.searchBy == '3Years') {

        var year = new Date(new Date().setFullYear(new Date().getFullYear() - 3));
        var filterBy = year.getFullYear()

        let isAccountExist = await db.accounts.findAll({
          where: {
            account_id: id,
            year: {
              $gte: filterBy,
              $lte: new Date(),
            },
          },
        })

        console.log(isAccountExist)

        if (!isAccountExist) {
            throw new Error('Sorry account is not found..')
        }

        let accTransaction = await db.account_transactions.findAll({
          where: {
            account_id: id,
            year: {
              $gte: filterBy,
              $lte: new Date(),
            },
          },
        })

        if (!accTransaction.length) {
            throw new Error('Sorry transaction is not found...')
        }
        log.end()

        return accTransaction
    }

    if (query.searchBy == 'SinceInception') {

        let isAccountExist = await db.accounts.find({
            where: {
                account_id: id,
            }
        })

        if (!isAccountExist) {
            throw new Error('Sorry account is not found..')
        }

        let accTransaction = await db.account_transactions.findAll({
            where: {
                account_id: id,
            }
        })

        if (!accTransaction.length) {
            throw new Error('Sorry transaction is not found...')
        }
        log.end()
        
        return accTransaction
    }
}




exports.consolidateAccount = consolidateAccount
exports.consolidateValuation = consolidateValuation
exports.accountValuation = accountValuation
exports.consolidateAssets = consolidateAssets
exports.accountAssets = accountAssets
exports.consolidatePerformance = consolidatePerformance
exports.accountPerformance = accountPerformance