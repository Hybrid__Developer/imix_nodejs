'use strict'
const encrypt = require('../permit/crypto')
const url = require('config').get('pic').url
var rootPath = require('path');
var fs = require('fs')
var nodemailer = require('nodemailer')
const set = async (model, tableData, context) => {
    const log = context.logger.start('services/clients/set')
    //Start set data for address model
    if (model.address_type && model.address_type != 'string') {
        tableData.address_type = model.address_type
    }
    if (model.address1 && model.address1 != 'string') {
        tableData.address1 = model.address1
    }
    if (model.address2 && model.address2 != 'string') {
        tableData.address2 = model.address2
    }
    if (model.country && model.country != 'string') {
        tableData.country = model.country
    }
    if (model.city && model.city != 'string') {
        tableData.city = model.city
    }
    if (model.post_code && model.post_code != 'string') {
        tableData.post_code = model.post_code
    }
    if (model.deleted_at && model.deleted_at != 'string') {
        tableData.deleted_at = model.deleted_at
    }
    //End set data for address model

    // if (model.email && model.email != 'string') {
    //     let isUserExist = await get({ email: model.email }, context)
    //     if (isUserExist && (isUserExist.id !== tableData.id)) {
    //         throw new Error('There is already a registration with this email!')
    //     }
    //     tableData.email = model.email
    // }
    if (model.newPassword && model.newPassword != 'string') {
        tableData.password = model.newPassword
    }
    // if (model.newPassword && model.newPassword != 'string') {
    //     tableData.password = encrypt.getHash(model.newPassword, context)
    // }
    if (model.newMemorable && model.newMemorable != 'string') {
        tableData.memorable = encrypt.getHash(model.newMemorable, context)
    }
    // tableData.updatedAt = new Date()
    log.end()
    await tableData.save()
    return tableData
}
const build = async (model, context) => {
    const log = context.logger.start('services/clients/build')
    const address = await db.addresses.build({
        client_id: model.client_id,
        address_type: model.address_type,
        address1: model.address1,
        address2: model.address2,
        country: model.country,
        city: model.city,
        post_code: model.post_code,
        created_at: new Date()
    }).save()
    log.end()
    return address
}

const create = async (model, context) => {
    const log = context.logger.start('services/clients/create')
    // model.password = encrypt.getHash(model.password, context)
    let isUserExist = await get({ email: model.email }, context)
    if (isUserExist) {
        throw new Error('There is already a registration with this email!')
    }
    model.password = encrypt.getHash(model.password, context)
    const client = await build(model, context)
    log.end()
    return client
}
const get = async (query, context) => {
    const log = context.logger.start(`services/clients/get:${query}`)
    if (query.id) {
        const client = await db.clients.find({
            where: {
                id: query.id,
            }
        })
        log.end()
        return client
    }
    if (query.email) {
        const client = await db.clients.find({
            where: {
                email: query.email
            }
        })
        log.end()
        return client
    }
    log.end()
    return null
}

const login = async (model, context) => {
    const log = context.logger.start('services/clients/login')

    const client = await db.clients.find({ where: { username: model.username } })

    if (!client) {
        log.end()
        throw new Error('Invalid username or password')
    }

    const isMemorableMatched = encrypt.compareHash(model.memorable, client.memorable, context)
    if (!isMemorableMatched) {
        // log.end()
        throw new Error('Invalid login credentials')
    }

    //const isPasswordMatched = encrypt.compareHash(model.password, client.password, context)
    if (model.password) {
        let clientPassword = Array.from(model.password)
        let dbPassword = Array.from(client.password)
        if (dbPassword[1] == clientPassword[0] && dbPassword[4] == clientPassword[1] && dbPassword[6] == clientPassword[2]) {
            log.end()
            return client
        } else {
            throw new Error('Invalid login credentials')
        }
    } else {
        // log.end()
        throw new Error('Please enter the 2, 5, 7 characters of your password.')
    }
    // if (client) {
    //     client.last_login = new Date()
    //     client.save()
    // }

}
const addAddress = async (model, context) => {
    const log = context.logger.start('services/clients/addAddress')

    let isUserExist = await db.clients.find({
        where: {
            id: model.client_id,
        }
    })
    if (!isUserExist) {
        throw new Error('Sorry client is not found..')
    }
    const address = await build(model, context)
    log.end()
    return address
}
const updateAddress = async (id, model, context) => {
    const log = context.logger.start(`services/clients/updateAddress/model:${model}`)
    let isAddressExist = await db.addresses.find({
        where: {
            id: id,
        }
    })
    if (!isAddressExist) {
        throw new Error('Sorry address is not found.. :(')
    }
    let addressdetail = await set(model, isAddressExist, context)
    log.end()
    return addressdetail
}
const addressList = async (id, model, context) => {
    const log = context.logger.start(`services/clients/addressList/model:${model}`)

    let isUserExist = await db.clients.find({
        where: {
            id: id,
        }
    })
    if (!isUserExist) {
        throw new Error('Sorry client is not found..')
    }

    let allAddress = await db.addresses.findAll({
        where: {
            client_id: id,
            deleted_at: null
        }
    })
    if (!allAddress.length) {
        throw new Error('Sorry address is not found...')
    }
    log.end()
    return allAddress
}
const forgotPasswordOtp = async (model, context) => {
    const log = context.logger.start(`services/clients/forgotPasswordOtp/`)

    let isUserExist = await db.clients.find({
        where: {
            email: model.email,
        }
    })
    if (!isUserExist) {
        throw new Error('Sorry client is not found...')
    }
    let otp = Math.floor(1000 + Math.random() * 9000)
    //Integrate email
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'php.mspl@gmail.com',
            pass: 'Strong@123'
        }
    });
    var mailOptions = {
        //from: 'sankhyan.amit@gmail.com',
        to: model.email,
        subject: 'Forgot Password',
        text: 'Your forgot password OTP is: ' + otp
    };
    transporter.sendMail(mailOptions);
    //end email integration
    log.end()
    var modifyotp = {otp: otp}
    modifyotp.client_id = isUserExist.id
    return modifyotp
}
const resetPassword = async (model, context) => {
    const log = context.logger.start(`services/clients/resetPassword/`)

    let isUserExist = await db.clients.find({
        where: {
            id: model.clientId,
        }
    })
    if (!isUserExist) {
        throw new Error('Sorry client is not found...')
    }

    const isMemorableMatched = encrypt.compareHash(model.memorable, isUserExist.memorable, context)
    if (!isMemorableMatched) {
        // log.end()
        throw new Error('Memorable word mismatch')
    }
    let forgotPassword = await set(model, isUserExist, context)

    log.end()
    return forgotPassword
}
const changePassword = async (model, context) => {
    const log = context.logger.start(`services/clients/changePassword/`)

    let isUserExist = await db.clients.find({
        where: {
            id: model.clientId,
        }
    })
    if (!isUserExist) {
        throw new Error('Sorry client is not found...')
    }

    //const isPasswordMatched = encrypt.compareHash(model.oldPassword, isUserExist.password, context)
    if (model.oldPassword != isUserExist.password) {
        throw new Error('Password mismatch')
    }
    const isMatchMemorable = encrypt.compareHash(model.oldMemorable, isUserExist.memorable, context)
    if (!isMatchMemorable) {
        throw new Error('Memorable mismatch')
    }

    let changePassword = await set(model, isUserExist, context)

    log.end()
    return changePassword
}
const imageUploader = async (model, id, context) => {
    let log = context.logger.start('services:clients:uploadImage')
    // var filePath = appRoot + '/' + upload.file.filename
    let date = new Date()
    date = date.getTime()
    var appDir = rootPath.dirname(require.main.filename);
    //console.log(appDir);
    //var filePath = appDir + `/profile/` + date
    var filePath = `profile/` + date

    let client = await db.clients.findById(id)
    if (!client) {
        log.end()
        throw new Error('USER IS NOT FOUND')
    }
    let picUrl = client.image
    if (picUrl && picUrl.includes(`${url}`)) {
        picUrl = picUrl.replace(`${url}`, '');
        try {
            fs.unlinkSync(`${picUrl}`)
            console.log('File unlinked!');
        } catch (err) {
            console.log(err)
        }
    }
    if (Object.keys(model.files).length == 0) {
        throw new Error('No files were uploaded.')
    }
    // /projects/node_foodver_backend
    let path = filePath + model.files.image.name
    model.files.image.mv(path, function (err) {
        if (err) {
            log.end()
            throw new Error('No files were uploaded.');
        }
        console.log('File uploaded!');
    });
    let completeURl = url + path
    client.image = completeURl
    client.save()
    log.end()
    return client
}
const deleteAddress = async (id, model, context) => {
    const log = context.logger.start(`services/clients/updateAddress/model:${model}`)
    let isAddressExist = await db.addresses.find({
        where: {
            id: id,
        }
    })
    if (!isAddressExist) {
        throw new Error('Sorry address is not found.. :(')
    }
    let addressdetail = await set(model, isAddressExist, context)
    log.end()
    return addressdetail
}








const update = async (id, model, context) => {
    const log = context.logger.start(`services/clients/update/model:${model}`)
    let isUserExist = await get({ id: id }, context)
    if (!isUserExist) {
        throw new Error('There is no client found with this email!')
    }
    let clientdetail = await set(model, isUserExist, context)
    log.end()
    return clientdetail
}

exports.create = create
exports.get = get
exports.login = login
exports.update = update
exports.imageUploader = imageUploader
exports.addAddress = addAddress
exports.updateAddress = updateAddress
exports.addressList = addressList
exports.resetPassword = resetPassword
exports.forgotPasswordOtp = forgotPasswordOtp
exports.changePassword = changePassword
exports.deleteAddress = deleteAddress