module.exports = [
    {
        url: '/consolidateAccount/{id}',
        get: {
            summary: 'Client consolidated accounts',
            description: 'all account',
            parameters: [{
                name: 'id',
                in: 'path',
                description: 'client id',
                required: true,
                type: "string"
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/consolidateValuation/{id}',
        get: {
            summary: 'Client consolidated valuation',
            description: 'all account valuation',
            parameters: [{
                name: 'id',
                in: 'path',
                description: 'client id',
                required: true,
                type: "string"
            }],
            responses: {
                default: {
                    description: 'investment_type: 1=>Fixed interest, 2=>Equities, 3=>Alternatives, 4=>commodities, 5=>cash',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/accountValuation/{id}',
        get: {
            summary: 'Client account valuation',
            description: 'single account valuation',
            parameters: [{
                name: 'id',
                in: 'path',
                description: 'account id',
                required: true,
                type: "string"
            }],
            responses: {
                default: {
                    description: 'investment_type: 1=>Fixed interest, 2=>Equities, 3=>Alternatives, 4=>commodities, 5=>cash',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/consolidateAssets',
        get: {
            summary: 'Client consolidate assets allocation',
            description: 'all assets allocation',
            parameters: [{
                in: 'query',
                // type: "integer",
                name: 'id',
                description: 'Client id',
                required: true,
            }, {
                in: 'query',
                // type: "integer",
                name: 'searchBy',
                description: 'searchBy',
                required: true,
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/accountAssets',
        get: {
            summary: 'Client account assets allocation',
            description: 'Assets allocation',
            parameters: [{
                in: 'query',
                // type: "integer",
                name: 'id',
                description: 'account id',
                required: true,
            }, {
                in: 'query',
                // type: "integer",
                name: 'searchBy',
                description: 'searchBy',
                required: true,
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/consolidatePerformance',
        get: {
            summary: 'Client consolidate performance',
            description: 'Consolidate Performance',
            parameters: [{
                in: 'query',
                // type: "integer",
                name: 'id',
                description: 'client id',
                required: true,
            }, {
                in: 'query',
                // type: "integer",
                name: 'searchBy',
                description: 'searchBy',
                required: true,
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/accountPerformance',
        get: {
            summary: 'Client account performance',
            description: 'Account Performance',
            parameters: [{
                in: 'query',
                // type: "integer",
                name: 'id',
                description: 'account id',
                required: true,
            }, {
                in: 'query',
                // type: "integer",
                name: 'searchBy',
                description: 'searchBy',
                required: true,
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
]