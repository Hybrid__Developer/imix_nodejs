module.exports = [
    {
        url: '/login',
        post: {
            summary: 'sign In or login',
            description: 'signIn with email',
            parameters: [{
                name: 'body',
                in: 'body',
                description: '',
                required: true,
                schema: {
                    '$ref': '#/definitions/clientsLoginReq'
                }
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/addAddress',
        post: {
            summary: 'Add more address',
            description: 'add multiline address',
            parameters: [{
                name: 'body',
                in: 'body',
                description: '',
                required: true,
                schema: {
                    '$ref': '#/definitions/addAddress'
                }
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/updateAddress/{id}',
        put: {
            summary: 'Update address',
            description: 'update multiline address',
            parameters: [
                {
                    in: "path",
                    name: "id",
                    description: "address id",
                    required: true,
                    type: "string"
                },
                {
                    name: 'body',
                    in: 'body',
                    description: '',
                    required: true,
                    schema: {
                        '$ref': '#/definitions/updateAddress'
                    }
                }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/addressList/{id}',
        get: {
            summary: 'Client address list',
            description: 'all address',
            parameters: [{
                name: 'id',
                in: 'path',
                description: 'client id',
                required: true,
                type: "string"
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/forgotPasswordOtp',
        post: {
            summary: 'Send Otp for forgot password',
            description: 'OTP for forgot Password',
            parameters: [{
                name: 'body',
                in: 'body',
                description: 'enter client email',
                required: true,
                schema: {
                    '$ref': '#/definitions/forgotPasswordOtp'
                }
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/resetPassword',
        post: {
            summary: 'Forgot password',
            description: 'Set new password',
            parameters: [{
                name: 'body',
                in: 'body',
                description: '',
                required: true,
                schema: {
                    '$ref': '#/definitions/resetPassword'
                }
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/changePassword',
        post: {
            summary: 'Change password',
            description: 'Set new password',
            parameters: [{
                name: 'body',
                in: 'body',
                description: '',
                required: true,
                schema: {
                    '$ref': '#/definitions/changePassword'
                }
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/imageUploader/{id}',
        put: {
            summary: 'Upload profile image',
            description: 'update profile image',
            parameters: [
                {
                    in: "path",
                    name: "id",
                    description: "Client id",
                    required: true,
                    type: "string"
                },
                {
                    name: 'image',
                    in: 'formData',
                    type: 'file',
                    description: 'The file to upload',
                    required: true,
                    // schema: {
                    //     '$ref': '#/definitions/imageUploader'
                    // }
                }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/deleteAddress/{id}',
        put: {
            summary: 'Delete address',
            description: 'Delete address',
            parameters: [
                {
                    in: "path",
                    name: "id",
                    description: "address id",
                    required: true,
                    type: "string"
                },
                {
                    name: 'body',
                    in: 'body',
                    description: 'pass current date',
                    required: true,
                    schema: {
                        '$ref': '#/definitions/deleteAddress'
                    }
                }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },

]