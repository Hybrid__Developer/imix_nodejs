module.exports = [
    {
        name: "deleteAddress",
        properties: {
            deleted_at: { type: "string", default: "" },
        }
    }
];
