module.exports = [
    {
        name: "updateAddress",
        properties: {
            address_type: { type: "string", default: "" },
            address1: { type: "string", default: "" },
            address2: { type: "string", default: "" },
            country: { type: "string", default: "" },
            city: { type: "string", default: "" },
            post_code: { type: "string", default: "" },
        }
    }
];
