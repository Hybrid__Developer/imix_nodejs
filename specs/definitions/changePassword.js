module.exports = [
    {
        name: "changePassword",
        properties: {
            clientId: { type: "string", default: "" },
            oldPassword: { type: "string", default: "" },
            newPassword: { type: "string", default: "" },
            confirmPassword: { type: "string", default: "" },
            oldMemorable: { type: "string", default: "" },
            newMemorable: { type: "string", default: "" },
            confirmMemorable: { type: "string", default: "" },
        }
    }
];