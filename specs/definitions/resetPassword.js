module.exports = [
    {
        name: "resetPassword",
        properties: {
            clientId: { type: "string", default: "" },
            newPassword: { type: "string", default: "" },
            confirmPassword: { type: "string", default: "" },
            memorable: { type: "string", default: "" },
        }
    }
];