'use strict'

const jwt = require('jsonwebtoken')
const authConfig = require('config').get('auth')
const models = require('../models')
const getToken = (id, isExpired, context) => {
    const log = context.logger.start(`permit:auth:getToken:${id}`)

    const extractFrom = {
        id: id,
        // device: device
    }

    const options = {}

    if (isExpired) {
        options.expiresIn = '5m'
    }

    const token = jwt.sign(extractFrom, authConfig.jwtKey, options)
    // log.end()
    return token
}

const extractToken = (token, context) => {
    const log = context.logger.start(`permit:auth:requiresToken:${token}`)

    try {
        const decoded = jwt.verify(token, authConfig.jwtKey)
        log.end()
        return decoded
    } catch (err) {
        log.end()
        return err
    }
}
const tryLogin = async (email, password, models, SECRET, SECRET_2) => {
    const client = await models.clients.findOne({ where: { email }, raw: true });
    if (!client) {
        throw new Error('Invalid login');
    }
    if (!client.confirmed) {
        throw new Error('Please confirm your email to login');
    }
    const valid = await bcrypt.compare(password, client.password);
    if (!valid) {
        throw new Error('Invalid login');
    }
}
// exports.requiresApp = (req, res, next) => {
//     let appCode = req.body.appCode || req.query.appCode || req.headers['x-app-code']

//     if (!appCode) { return res.failure('x-app-code is required.') }

//     db.app.findOne({
//         where: { code: appCode }
//     }).then((app) => {
//         if (!app) { return res.failure('app does not exist') }
//         req.app = app
//         next()
//     })
// }

exports.getToken = getToken
exports.extractToken = extractToken
exports.tryLogin = tryLogin