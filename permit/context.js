'use strict'
const auth = require('./auth')
const response = require('../exchange/response')

const builder = (req, res, next) => {
    const context = {
        logger: require('@open-age/logger')('permit:context:builder')
    }

    req.context = context
    if (next) {
        return next()
    }
    return null
}

const validateToken = (req, res) => {
    builder(req, res)
    const log = req.context.logger.start(`permit:auth:validateToken`)

    const token = req.headers['x-access-token']

    if (!token) {
        return response.failure(res, 'token is required')
    }

    const details = auth.extractToken(token, req.context)

    if (details.name === 'TokenExpiredError') {
        return response.failure(res, 'token expired')
    }

    if (details.name === 'JsonWebTokenError') {
        return response.failure(res, 'token is invalid')
    }
    log.end()

    return response.success(res, 'token is valid')
}

const requiresToken = async (req, res, next) => {
    builder(req, res)
    const log = req.context.logger.start(`permit:auth:requiresToken`)

    const token = req.headers['x-access-token']

    if (!token) {
        return response.failure(res, 'token is required')
    }

    const decodedUser = auth.extractToken(token, req.context)

    const client = await db.clients.findByPk(decodedUser.id)
    if (client) {
        if (!client.confirmed) {
            return response.failure(res, 'unverified client')
        }

        if (!client.active) {
            return response.failure(res, 'invalid client')
        }

        if (client.token !== token) {
            return response.unAuthorized(res, 'unAuthorized User')
        }

        // const deviceExists = client.devices.find((device) => {
        //     return device.id === decodedUser.device
        // })

        // if (!deviceExists) {
        //     return response.failure(res, 'token expired')
        // }

        req.context.client = client
        log.end()
        return next()
    }

    // const driver = await db.drivers.findByPk(decodedUser.id)
    // if (driver) {
    //     if (!driver.isVerified) {
    //         return response.failure(res, 'Unverified driver')
    //     }

    //     if (driver.status.toLowerCase() === 'inactive') {
    //         return response.failure(res, 'Your account is under review, we will get back to you when your profile is completed. Please contact, support team for more info')
    //     }

    //     if (driver.isDeleted) {
    //         return response.failure(res, 'Invalid driver please contact support')
    //     }

    //     if (driver.isSuspended) {
    //         return response.failure(res, driver.comments)
    //     }

    //     if (driver.isRejected) {
    //         return response.failure(res, 'Your documents for verification is rejected, please submit your documents again for verification')
    //     }

    //     req.context.driver = driver
    //     log.end()
    //     return next()
    // }

    // const vendor = await db.vendors.findByPk(decodedUser.id)
    // if (vendor) {
    //     req.context.vendor = vendor
    //     log.end()
    //     return next()
    // }

    return response.unAuthorized(res, 'invalid token')
}

exports.builder = builder
exports.requiresToken = requiresToken
exports.validateToken = validateToken
