'use strict'

var fs = require('fs')
var specs = require('../specs')
var auth = require('../permit')
var configOfLogger = require('config').get('logger')
const fileUpload = require('express-fileupload');
var apiRoutes = require('@open-age/express-api')
var appRoot = require('app-root-path')
const apis = require('../api')
const configure = (app, logger) => {
    logger.start('settings/routes:configure')

    let specsHandler = function (req, res) {
        fs.readFile('./public/specs.html', function (err, data) {
            if (err) {
                res.writeHead(404)
                res.end()
                return
            }
            res.contentType('text/html')
            res.send(data)
        })
    }
    app.get('/', specsHandler)

    app.get('/swagger', (req, res) => {
        res.writeHeader(200, {
            'Content-Type': 'text/html'
        })
        fs.readFile('./public/swagger.html', null, function (err, data) {
            if (err) {
                res.writeHead(404)
                res.end()
                return
            }
            res.write(data)
            res.end()
        })
    })
    // if(request.url === "/forgot-password-email"){
    //     fs.readFile("./email_templates/forgot-password-email.html", function (err, data) {
    //        response.writeHead(200, {'Content-Type': 'text/html'});
    //        response.write(data);
    //        response.end();
    //     });
    //  }
    let htmlHandler = function (req, res) {
        fs.readFile('email_templates/forgot-password-email.html', function (err, data) {
            if (err) {
                res.writeHead(404)
                res.end()
                return
            }
            res.contentType('text/html')
            res.send(data)
        })
    }
    app.get('/', htmlHandler)
    app.get('/api/specs', function (req, res) {
        res.contentType('application/json')
        res.send(specs.get())
    })

    app.get('/logs', function (req, res) {
        var filePath = appRoot + '/' + configOfLogger.file.filename

        fs.readFile(filePath, function (err, data) {
            if (err) {
                res.writeHead(404)
                res.end()
                return
            }
            res.contentType('application/json')
            res.send(data)
        })
    })

    app.use(fileUpload());
    // app.get('/resetForgotPassword/:resetPasswordToken', function(req, res) {
    //     User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, client) {
    //       if (!client) {
    //         req.flash('error', 'Password reset token is invalid or has expired.');
    //         return res.redirect('/forgot');
    //       }
    //       res.render('reset', {
    //         client: req.client
    //       });
    //     });
    //   });

    var api = apiRoutes(app)
    try {
        api.model('clients').register([{
            action: 'POST',
            method: 'login',
            url: '/login',
            filter: auth.context.builder
        },
        {
            action: 'POST',
            method: 'addAddress',
            url: '/addAddress',
            filter: auth.context.builder
        },
        {
            action: 'PUT',
            method: 'updateAddress',
            url: '/updateAddress/:id',
            filter: auth.context.builder
        },
        {
            action: 'GET',
            method: 'addressList',
            url: '/addressList/:id',
            filter: auth.context.builder
        },
        {
            action: 'POST',
            method: 'forgotPasswordOtp',
            url: '/forgotPasswordOtp',
            filter: auth.context.builder
        },
        {
            action: 'POST',
            method: 'resetPassword',
            url: '/resetPassword',
            filter: auth.context.builder
        },
        {
            action: 'POST',
            method: 'changePassword',
            url: '/changePassword',
            filter: auth.context.builder
        },
        {
            action: 'PUT',
            method: 'imageUploader',
            url: '/imageUploader/:id',
            filter: auth.context.builder
        },
        {
            action: 'PUT',
            method: 'deleteAddress',
            url: '/deleteAddress/:id',
            filter: auth.context.builder
        },
        ])
        api.model('accounts').register([{
            action: 'GET',
            method: 'consolidateAccount',
            url: '/consolidateAccount/:id',
            filter: auth.context.builder
        },
        {
            action: 'GET',
            method: 'consolidateValuation',
            url: '/consolidateValuation/:id',
            filter: auth.context.builder
        },
        {
            action: 'GET',
            method: 'accountValuation',
            url: '/accountValuation/:id',
            filter: auth.context.builder
        },
        {
            action: 'GET',
            method: 'consolidateAssets',
            url: '/consolidateAssets',
            filter: auth.context.builder
        },
        {
            action: 'GET',
            method: 'accountAssets',
            url: '/accountAssets',
            filter: auth.context.builder
        },
        {
            action: 'GET',
            method: 'consolidatePerformance',
            url: '/consolidatePerformance',
            filter: auth.context.builder
        },
        {
            action: 'GET',
            method: 'accountPerformance',
            url: '/accountPerformance',
            filter: auth.context.builder
        },
        ])
    } catch (err) {
        log.error(err)
    }
    logger.end()
}

exports.configure = configure
