

'use strict'


const logger = require('@open-age/logger')('database')
const dbConfig = require('config').get('db')
global.Sequelize = require('sequelize')
const webServerConfig = require('config').get('webServer')

module.exports.configure = () => {
    const sequelize = new Sequelize(
        dbConfig.database,
        dbConfig.username,
        dbConfig.password,
        {
            host: dbConfig.host,
            // port: dbConfig.port,
            dialect: dbConfig.dialect,
            logging: false,
            define: {
                timestamps: false
            }
        }
    )

    global.sequelize = sequelize
    global.db = require('../models')
    sequelize.sync().then(() => {
        console.log('db connected')
        var port = process.env.PORT || webServerConfig.port
        console.log('port:')
        console.log(`localhost:${port}`)
    }).catch(function (err) {
        console.log(err)
        console.log('DB Connection Failed')
    })
}