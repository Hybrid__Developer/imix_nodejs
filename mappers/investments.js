'use strict'

exports.toModel = (entity) => {
    let marketTotal = 0
    let bookTotal = 0
    let yieldTotal = 0
    if (entity.investments) {
        if (entity.investments.length > 0) {
            entity.investments.forEach(element => {
                marketTotal += element.market_value
                bookTotal += element.book_cost
                yieldTotal += element.projected_yield
            })
        }
    }

    const model = {
        investment_type: entity.name,
        investments: entity.investments,
    }

    if (entity.investments) {
        if (entity.investments.length > 0) {
            model.marketTotal = marketTotal,
                model.bookTotal = bookTotal,
                model.yieldTotal = ((marketTotal - bookTotal) / bookTotal * 100).toFixed(2)
            //model.yieldTotal = yieldTotal
        }
    }

    if (entity.bookGrandTotal) {
        model.grandTotal = {
            bookGrandTotal: entity.bookGrandTotal,
            marketGrandTotal: entity.marketGrandTotal,
            yieldGrandTotal: entity.yieldGrandTotal,
        }

    }
    return model
}

exports.toSearchModel = (entities) => {
    return entities.map((entity) => {
        return exports.toModel(entity)
    })
}