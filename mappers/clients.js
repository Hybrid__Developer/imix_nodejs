'use strict'

exports.toModel = (entity) => {
    const model = {
        id: entity.id,
        name: entity.name,
        clientname: entity.clientname,
        client_id: entity.client_id,
        image: entity.image,
        email: entity.email,
        created_at: entity.created_at,
        search_key: entity.search_key,
        currency: entity.currency,
        nationality: entity.nationality,
        gender: entity.gender,
        dob: entity.dob,
        cgt_enabled: entity.cgt_enabled,
        start_date: entity.start_date,
        closed_date: entity.closed_date,
        inception_date: entity.inception_date,
        mifid_type: entity.mifid_type,
        mifid_category: entity.mifid_category,
        ni_number: entity.ni_number,
        deceased_date: entity.deceased_date,
        marital_status: entity.marital_status,
        w8ben: entity.w8ben,
        revision_marker: entity.revision_marker,
        tax_rate: entity.tax_rate,
        risk_profile_id: entity.risk_profile_id,
        client_type: entity.client_type,
        mifid_identifier_type: entity.mifid_identifier_type,
        lei: entity.lei,
        national_id: entity.national_id,
        concat: entity.concat
    }
    return model
}

exports.toSearchModel = (entities) => {
    return entities.map((entity) => {
        return exports.toModel(entity)
    })
}
