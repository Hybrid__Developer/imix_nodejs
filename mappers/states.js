'use strict'

exports.toModel = (entity) => {
    const model = {
        id: entity.id,
        name: entity.name,
        uf: entity.uf
    }
    return model
}

exports.toSearchModel = (entities) => {
    return entities.map((entity) => {
        return exports.toModel(entity)
    })
}
