'use strict'

exports.toModel = (entity) => {
    const model = {
        id: entity.id,
        client_id: entity.client_id,
        account_id: entity.account_id,
        account_name: entity.account_name,
        total_amount: entity.total_amount
    }
    return model
}

exports.toSearchModel = (entities) => {
    return entities.map((entity) => {
        return exports.toModel(entity)
    })
}
