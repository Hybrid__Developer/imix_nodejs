'use strict'

exports.update = (toUpdateModel, existingModel) => { 
    for (var key in toUpdateModel) {
        existingModel[key] = toUpdateModel[key] 
    }
    return existingModel
}